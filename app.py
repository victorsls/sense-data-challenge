from flask import Flask, render_template, request
from flask_paginate import Pagination, get_page_parameter

from forms import CharactersFilterForm
from utils import get_characters, get_all_starships, format_starships, sort_list_of_dict_by_key

app = Flask(__name__)

app.config['SECRET_KEY'] = 'SECRET_KEY'


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/characters', methods=['GET', 'POST'])
def characters_list():
    page = request.args.get(get_page_parameter(), type=int, default=1)
    per_page = 10
    form = CharactersFilterForm()
    if form.validate_on_submit():
        characters = get_characters(page=page)
        characters['results'] = sort_list_of_dict_by_key(characters.get('results'), form.sort_by.data)
    else:
        characters = get_characters(page=page)
    pagination = Pagination(page=page, per_page=per_page, total=characters.get('count'), record_name='characters',
                            css_framework='bootstrap4')
    return render_template('characters.html', form=form, characters=characters.get('results'), pagination=pagination)


@app.route('/starships', methods=['GET'])
def starships_list():
    return render_template('starships.html', starships=format_starships(get_all_starships()))
