from flask_wtf import FlaskForm
from wtforms import SelectField


class CharactersFilterForm(FlaskForm):
    SORT_CHOICES = [('name', 'Nome'), ('gender', 'Gênero'), ('mass', 'Peso'), ('height', 'Altura')]
    sort_by = SelectField(
        'Ordenar Por', choices=SORT_CHOICES,
        render_kw={'class': 'custom-select my-1 mr-sm-2'}
    )