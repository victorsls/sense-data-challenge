FROM python:3.6

LABEL Author="Victor Rafael"
LABEL E-mail="victorliraserafim@gmail.com"
LABEL version="1.0"

ENV PYTHONDONTWRITEBYTECODE=1
ENV FLASK_APP=app.py
ENV FLASK_ENV=development
ENV FLASK_DEBUG=True

RUN mkdir /app
WORKDIR /app

COPY ./requirements.txt /app/

RUN pip install --upgrade pip && pip install -r  requirements.txt

ADD . /app

EXPOSE 5000

CMD flask run --host=0.0.0.0
