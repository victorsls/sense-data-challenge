import requests


def sort_list_of_dict_by_key(list_of_dict, key, reverse=False):
    return sorted(list_of_dict, key=lambda obj: obj[key], reverse=reverse)


def get_characters(page):
    return base_api_swapi_co('people', params={'page': page})


def base_api_swapi_co(path='', params=None, url=None):
    if not url:
        url = f'https://swapi.co/api/{path}/'
    response = requests.request('GET', url, params=params)
    if response.status_code == 200:
        response = response.json()
    return response


def get_all_starships():
    starships = []
    resp = base_api_swapi_co(url='https://swapi.co/api/starships/')
    starships += resp.get('results')
    while resp.get('next'):
        resp = base_api_swapi_co(url=resp.get('next'))
        starships += resp.get('results')
    return starships


def format_starships(starships):
    for starship in starships:
        hyperdrive_rating = starship.get('hyperdrive_rating')
        if not hyperdrive_rating or hyperdrive_rating == 'unknown':
            starship['score'] = 0
            continue

        cost_in_credits = starship.get('cost_in_credits')
        if not cost_in_credits or cost_in_credits == 'unknown':
            starship['score'] = 0
            continue
        score = float(hyperdrive_rating) / float(cost_in_credits)
        starship['score'] = score
    return sort_list_of_dict_by_key(starships, 'name', True)
